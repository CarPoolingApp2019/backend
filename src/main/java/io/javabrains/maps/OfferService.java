package io.javabrains.maps;
import io.javabrains.users.*;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Responses.LoginResponse;
import io.javabrains.users.Token;
import io.javabrains.users.User;
import io.javabrains.users.UserRepository;

@Service
public class OfferService {
	@Autowired
	private RequestRepository requestrepo;
	@Autowired
	private OfferRepository offerrepository;
	@Autowired
	private UserRepository userrepo;
	@Autowired
	private TripRepository triprepo;
	@Autowired
	private VehicleRepository vehiclerepo;
	public List<Offer> getAllOffer(){
		List<Offer> offers = new ArrayList<>();
		offerrepository.findAll()
		.forEach(offers :: add);
		
		return offers;

}
public Offer AddOffer(AddOfferobj addofferObj) {
	System.out.println("inside offer service: "+addofferObj.getRegisteration_token());
	long user_id= userrepo.findUser(addofferObj.getRegisteration_token()).getId();
	User user = new User(user_id);
	Place place = new Place(addofferObj.getDestination_place_id());
	System.out.println("offer Trip" + addofferObj.getRegisteration_token());
	int vehicle_id=vehiclerepo.findVehicle(addofferObj.getRegisteration_token()).getId();
	Vehicle vehicle=new Vehicle(vehicle_id);
	Offer offer = new Offer(addofferObj.getLatitude() ,addofferObj.getLatitude(),
			addofferObj.getTrip_actual_time(),addofferObj.getAddress(),place,user,vehicle);
	
	offerrepository.save(offer);
	return offer;
	
		
}
public void cancleOffer(int offer_id) {

	if(triprepo.selectTripAttatchedToSpecificOffer(offer_id).size() == 0) {
		System.out.print("triprepo case1");
		offerrepository.DeleteOffer(offer_id);
	}else {
		System.out.print("triprepo case2");
		long tripId = triprepo.selectTripAttatchedToSpecificOffer(offer_id).get(0).getId();
		requestrepo.unconnectRequestToTrip(tripId);
	       triprepo.deleteTrip(tripId);
		offerrepository.DeleteOffer(offer_id);
	
	}
	
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}