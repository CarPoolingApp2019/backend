package io.javabrains.users;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Responses.LoginResponse;

@Service
public class UserService {
	@Autowired
	private UserRepository userrepository;
	@Autowired
	private TokenRepository tokenRepo;

	public List<User> getAllUsers(){
		List<User> users = new ArrayList<>();
		userrepository.findAll()
		.forEach(users :: add);
		System.out.println("sabrin" + users.get(0).getFirst_name());
		System.out.println("Hi"+users.get(0).getTokens());
		return users;
	}

	public LoginResponse login(String phone) throws GlobalControllerExceptionHandler {
		List<User> users = userrepository.findUserByPhone(phone);
		if(users.size()<=0) 
			throw new GlobalControllerExceptionHandler();
		Token myToken = new Token(users.get(0));
		tokenRepo.save(myToken);
		LoginResponse response = new LoginResponse(users.get(0) , myToken.getToken());
		return response;
		
			}

	
	public LoginResponse addUser(User user) {
		System.out.println("userService"+ user.getFirst_name());
		
Token myToken = new Token(user);
User newuser = new User(user.getFirst_name() , user.getSecond_name(),user.getPhone() , user.getGender(),user.getEmail(),myToken);
userrepository.save(newuser);
tokenRepo.save(myToken);
LoginResponse response = new LoginResponse(user , myToken.getToken());
		return response;
	}


}
