package io.javabrains.maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="place")
public class Place {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private int id;
private double lon;
private double lat;
private String name;

//relation with city
@ManyToOne
@JoinColumn(name = "city_code")
private City city;
@JsonIgnore
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
@OneToMany(mappedBy = "place", fetch = FetchType.EAGER,cascade = CascadeType.ALL)
private Set<Offer> offers ;
@JsonIgnore
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
@OneToMany(mappedBy = "place", fetch = FetchType.EAGER,cascade = CascadeType.ALL)
private Set<Request> request ;
public Place(double lon,double lat,String name,City city) {
	this.lon=lon;
	this.lat=lat;
	this.name=name;
	this.city=city;
   
}
public Place() {
	
}

public Place(int id) {
	this.id =id;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}




public double getLon() {
	return lon;
}

public void setLon(double lon) {
	this.lon = lon;
}

public double getLat() {
	return lat;
}

public void setLat(double lat) {
	this.lat = lat;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public City getCity() {
	return city;
}




public void setCity(City city) {
	this.city = city;
}




public Set<Offer> getOffers() {
	return offers;
}




public void setOffers(Set<Offer> offers) {
	this.offers = offers;
}




public Set<Request> getRequest() {
	return request;
}




public void setRequest(Set<Request> request) {
	this.request = request;
}







}
