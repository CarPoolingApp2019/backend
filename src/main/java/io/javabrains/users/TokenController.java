package io.javabrains.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TokenController {
@Autowired
TokenService tokenservice;
    @RequestMapping(value="/sendTokens", method=RequestMethod.POST)
	public void addDeviceNotificationToken(@RequestParam String  notification_token, @RequestParam String registeration_token) {
		tokenservice.addDeviceNotificationToken(notification_token,registeration_token);
		
	}
}
