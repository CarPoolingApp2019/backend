package io.javabrains.users;
import io.javabrains.maps.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
@Entity
@Table(name="users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
   // @SequenceGenerator(name="users_id_seq", sequenceName="users_id_seq", allocationSize=1)
    
	private long id;
    private String first_name;
    private String second_name;
    @Column(unique=true)
    private String phone;
    private String gender;
    private String email;
  @JsonIgnore
    @JsonIgnoreProperties("user")
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Set<Token> tokens;
  @JsonIgnore
  @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
  @OneToMany(mappedBy = "user", fetch = FetchType.EAGER,cascade = CascadeType.ALL)
 
  private Set<Offer> offers ;
  @JsonIgnore
  @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
  @OneToMany(mappedBy = "user", fetch = FetchType.EAGER,cascade = CascadeType.ALL)
 
  private Set<Request> request ;
  @JsonIgnore
  @JsonIgnoreProperties("user")
  @OneToMany(mappedBy = "user", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
  private Set<Vehicle> vehicle;
  
    public User(String first_name,String second_name, String phone, String gender,String email,Token... tokens) {
        this.first_name = first_name;
        this.second_name = second_name;
        this.phone= phone;
        this.gender=gender;
        this.email=email;
       
        //relationship with token
        this.tokens = Stream.of(tokens).collect(Collectors.toSet());
        this.tokens.forEach(x -> x.setUser(this));
    }
    
    public User(long id) {
   this.id=id;
    }
   
    

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public User() {
    	
    }
    
//	public User(long id, String first_name, String second_name, String phone, String gender) {
//		super();
//		this.id = id;
//		this.first_name = first_name;
//		this.second_name = second_name;
//		this.phone = phone;
//		this.Gender = gender;
//	}
	public long getId() {
		return id;
	}
	public Set<Token> getTokens() {
		return tokens;
	}

	public void setTokens(Set<Token> tokens) {
		this.tokens = tokens;
	}

	public void setId(long id) {
		this.id = id;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getSecond_name() {
		return second_name;
	}
	public void setSecond_name(String second_name) {
		this.second_name = second_name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}




	public Set<Offer> getOffers() {
		return offers;
	}




	public void setOffers(Set<Offer> offers) {
		this.offers = offers;
	}




	public Set<Request> getRequest() {
		return request;
	}




	public void setRequest(Set<Request> request) {
		this.request = request;
	}

	public Set<Vehicle> getVehicle() {
		return vehicle;
	}

	public void setVehicle(Set<Vehicle> vehicle) {
		this.vehicle = vehicle;
	}




		
}
