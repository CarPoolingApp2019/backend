package io.javabrains.firebase.service;

import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.javabrains.maps.Request;

// API so that heat comes on server API and we can see the response on the mobile device. 
@RestController
public class MobileController {
	private final String TOPIC = "Sabrin";

	@Autowired
	AndroidPushNotificationsService androidPushNotificationsService;
	@Autowired
	PassengersDeviceTokens passengerDevicetokens;

	@RequestMapping(value = "/send", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ProjectStatus> send(String deviceToken, String title, String body) throws JSONException {

		// example for request json
		/**
		 * { "notification": { "title": "JSA Notification", "body": "Happy Message!" },
		 * "data": { "Key-1": "JSA Data 1", "Key-2": "JSA Data 2" }, "to":
		 * "/topics/JavaSampleApproach", "priority": "high" }
		 */
		try {
			 AndroidPushNotificationsService.sendPushNotification(deviceToken, title, body);
			return new ResponseEntity<ProjectStatus>(HttpStatus.OK);
		} catch (IOException e) {

		}
		return new ResponseEntity<ProjectStatus>(HttpStatus.BAD_REQUEST);

	}

	@RequestMapping(value = "/sendNotifications", method = RequestMethod.POST)
	public List<String> sendNotifications(@RequestBody List<Request> selectedPassengers , long trip_id) {
		System.out.println("hello from sendNotifications");
		List<String> DeviceTokens = passengerDevicetokens.getpassengersTokens(selectedPassengers);
		System.out.println("notificationToken: " + DeviceTokens.get(0));
		String DriverName = passengerDevicetokens.getTripDriverName(trip_id);
		for (String device_token : DeviceTokens) {
			String message = "your request accepted and your ride will be with :" +""+DriverName;
			System.out.println("ya raaaaaaaaaaaaab"+message);
			send(device_token, "hello",message);
		}
		return DeviceTokens;
	}

}
