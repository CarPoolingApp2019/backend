package io.javabrains.maps;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import Responses.LoginResponse;

import io.javabrains.users.Token;
import io.javabrains.users.User;

@Controller
@RestController
public class TripController {
	
	@Autowired
	private TripService tripservice;
	@Autowired
	private TripService requestservice;
	@RequestMapping(value="/trip", method=RequestMethod.GET)
	public List<Trip> getAllPlace() {
		  return tripservice.getAllTrip();
	  }
	@RequestMapping(value="/history", method=RequestMethod.POST)
	
	public HistoryResponse findTripByRegestration_token(@RequestParam String registeration_token ){
		
	 return tripservice.findTripByRegestration_token(registeration_token);
	}



	@RequestMapping(value="/schedule", method=RequestMethod.POST)
	public ScheduleResponse findScheduleTripByRegestration_token(@RequestParam String registeration_token ){
		
		 return tripservice.findTripschedleByRegestration_token(registeration_token);
		}
	@RequestMapping(value="/startTime", method=RequestMethod.POST)
	public void setStart_time(@RequestParam long id,long start_time)	{
		tripservice.setStart_time(id,start_time);
	}
	@RequestMapping(value="/endTime", method=RequestMethod.POST)
	public void setEnd_time(@RequestParam long id,long end_time)	{
		tripservice.setEnd_time(id,end_time);
	}
	
	
}
