package io.javabrains.maps;

import io.javabrains.users.*;
import io.javabrains.maps.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Responses.LoginResponse;
@Service
public class TripService {
		@Autowired
		private TripRepository triprepository;
		@Autowired
		private RequestRepository requestrepository;
		@Autowired
		private OfferRepository offerrepo;
public List<Trip> getAllTrip(){
			List<Trip> trips = new ArrayList<>();
			triprepository.findAll()
			.forEach(trips :: add);
			System.out.println("hh" + trips.get(0));
			return trips;
}

public HistoryResponse findTripByRegestration_token(String registeration_token){
	List<Trip> trips = new ArrayList<Trip>();
	trips = triprepository.findTripByRegestration_token(registeration_token);
	List<UserTripResponse> userHistory = new ArrayList<UserTripResponse>();
	for(Trip trip : trips) {
	if(trip.getEnd_time()!=null && trip.getEnd_time()!=null ) {
		int IsOffer=1;
		UserTripResponse userTrip = new UserTripResponse(trip.getOffer().getAddress(), trip.getEnd_time(), trip.getStart_time(),trip.getOffer().getPlace().getName(),trip.getCost(),IsOffer,trip.getId(),trip.getOffer().getId());
		userHistory.add(userTrip);
	}}
   List<Request>tripreq=new ArrayList<Request>();
   tripreq= requestrepository.findRequestTripByRegestration_token(registeration_token)	;
List<UserTripResponse> userreq= new ArrayList<UserTripResponse>();
for(Request request :tripreq) {
	if(request.getTrip().getEnd_time()!=null && request.getTrip().getStart_time()!=null) {
		int IsOffer=0;
	UserTripResponse requesttrip= new UserTripResponse(request.getAddress() ,request.getTrip().getEnd_time(),request.getTrip().getStart_time(),request.getPlace().getName(),request.getTrip().getCost(),IsOffer,request.getTrip().getId(),request.getTrip().getOffer().getId());
	userreq.add(requesttrip);
}}
HistoryResponse historyresponse= new HistoryResponse(userHistory ,userreq);
return historyresponse;
}



public Trip addNewTrip(int offer_id) {
	Offer offer = offerrepo.getOfferById(offer_id);
	Trip newTrip = new Trip(null,null,offer,0);
	return triprepository.save(newTrip);
	
}

//scheduleTrip
public ScheduleResponse findTripschedleByRegestration_token(String registeration_token)
{
	List<Trip> trips = new ArrayList<Trip>();
	trips = triprepository.findTripByRegestration_token(registeration_token);
	List<UserTripResponse> Offerschedule = new ArrayList<UserTripResponse>();
	for(Trip trip : trips) {
	if(trip.getEnd_time()==null || trip.getStart_time()==null) {
		int IsOffer=1;
		UserTripResponse offerschedule = new UserTripResponse(trip.getOffer().getAddress(), trip.getEnd_time(), trip.getStart_time(),trip.getOffer().getPlace().getName(),
				trip.getCost(),IsOffer,trip.getId(),trip.getOffer().getId());
		Offerschedule.add(offerschedule);
	}}
	 List<Request>tripschdule=new ArrayList<Request>();
	 tripschdule= requestrepository.findRequestTripByRegestration_token(registeration_token)	;
	List<UserTripResponse> Requestschedule= new ArrayList<UserTripResponse>();
	for(Request request :tripschdule) {
		if(request.getTrip().getEnd_time()==null || request.getTrip().getStart_time() ==null) {
			int IsOffer=0;
			System.out.println("request.getAddress() "+request.getAddress());
		UserTripResponse requestschedule= new UserTripResponse(request.getAddress() ,request.getTrip().getEnd_time(),request.getTrip().getStart_time(),request.getPlace().getName(),request.getTrip().getCost(),IsOffer,request.getTrip().getId(),request.getTrip().getOffer().getId());
		 Requestschedule.add(requestschedule);
	}}

ScheduleResponse scheduleresponse= new ScheduleResponse(Offerschedule ,Requestschedule);
return scheduleresponse;

	
	}
public void setStart_time(long id,long start_time) {
	triprepository.setStart_time(id,start_time);
}
public void setEnd_time(long id,long end_time) {
	triprepository.setEnd_time(id,end_time);
}

}








