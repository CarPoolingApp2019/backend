package io.javabrains.maps;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class PlaceController {
	@Autowired
	private PlaceService placeservice;
	@RequestMapping(value="/place", method=RequestMethod.GET)
	public List<Place> getAllPlace() {
		  return placeservice.getAllPlaces();
	  }

	@RequestMapping(value="/nearby" ,method=RequestMethod.GET)
	public List<Place> findNearLocations(@RequestParam double lat , @RequestParam double lon){
		System.out.println("findNearbyLocations Api controller"+lat+"lon :"+lon);
		
		return placeservice.findNearLocations(lat, lon);
	}	
	
	
}
