package io.javabrains.users;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface VehicleRepository extends CrudRepository<Vehicle, Integer> {
	@Query(value="SELECT * FROM vehicle WHERE vehicle.id = :id",nativeQuery = true)
	public Vehicle selectUser(@Param("id")int id);
	@Modifying
	@Transactional
	@Query(value ="UPDATE vehicle SET defult_car= false WHERE (user_id= :user_id)",nativeQuery = true)
    public void setFalse(@Param ("user_id")long user_id);
	@Modifying
	@Transactional
	@Query(value =" UPDATE vehicle SET  defult_car=true WHERE vehicle.id= :id " ,nativeQuery = true)
	public void setDefultcar(@Param("id") int id);
	
	@Query(value ="SELECT vehicle.* "
	+"FROM vehicle "
	+"JOIN users ON vehicle.user_id=users.id "
	+"JOIN user_token ON user_token.user_id=users.id "
	+"WHERE (registeration_token= :registeration_token AND defult_car= true) "
		  ,nativeQuery = true) 
public Vehicle findVehicle(@Param("registeration_token")String registeration_token);
@Query(value ="SELECT vehicle.* "
			+"FROM vehicle "
			+"JOIN users ON vehicle.user_id=users.id "
			+"JOIN user_token ON user_token.user_id=users.id "
			+"WHERE (registeration_token= :registeration_token) "
				  ,nativeQuery = true) 
		public List<Vehicle> findVehicleByregisterayion_token(@Param("registeration_token")String registeration_token);
			
	
}