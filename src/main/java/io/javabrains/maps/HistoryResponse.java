package io.javabrains.maps;

import java.util.List;

public class HistoryResponse {
	List<UserTripResponse>Offerstrip;
	List<UserTripResponse>Requesttrips;
	
	
	
	
	public HistoryResponse(List<UserTripResponse> offerstrip, List<UserTripResponse> requesttrips) {
		
		Offerstrip = offerstrip;
		Requesttrips = requesttrips;
	}
	public List<UserTripResponse> getOfferstrip() {
		return Offerstrip;
	}
	public void setOfferstrip(List<UserTripResponse> offerstrip) {
		Offerstrip = offerstrip;
	}
	public List<UserTripResponse> getRequesttrips() {
		return Requesttrips;
	}
	public void setRequesttrips(List<UserTripResponse> requesttrips) {
		Requesttrips = requesttrips;
	}
	
	
	
	
	
}
