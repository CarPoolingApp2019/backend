package io.javabrains.users;
import io.javabrains.users.User;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
@Repository
public interface UserRepository extends CrudRepository<User, Long>{
	@Query(value = "SELECT * FROM users WHERE users.phone = :phone" ,nativeQuery = true)
	List<User> findUserByPhone(@Param("phone") String phone);
	
	@Query(value="SELECT users.* from user_token JOIN users ON user_token.user_id=users.id WHERE registeration_token= :registeration_token",nativeQuery = true)
	public User findUser(@Param("registeration_token")String registeration_token);
	
	@Query(value="SELECT * FROM users WHERE users.id=:userID",nativeQuery = true)
	public User findUserByID(@Param("userID") long userID);
	
	
}
