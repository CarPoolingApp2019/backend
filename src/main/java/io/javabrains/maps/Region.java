package io.javabrains.maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="region")
public class Region {
	@Id
private String code;
private String name;
@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "country_code")
private Country country;
@JsonIgnore
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
@OneToMany(mappedBy = "region",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
private Set<City> cities ;

public Region(String name,Country country) {
	
	this.name=name;
	
	this.country=country;

}



public Region () {
	  
}


public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public Country getCountry() {
	return country;
}
public void setCountry(Country country) {
	this.country= country;
}







public Set<City> getCities() {
	return cities;
}



public void setCities(Set<City> cities) {
	this.cities = cities;
}



public String getCode() {
	return code;
}



public void setCode(String code) {
	this.code = code;
}








}