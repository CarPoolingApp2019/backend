package io.javabrains.maps;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import Responses.LoginResponse;
import io.javabrains.users.User;
@RestController
public class RequestController {
	
	
		@Autowired
		private RequestService requestservice;
		@RequestMapping(value="/request", method=RequestMethod.GET)
		public List<Request> getAllRequest() {
			  return requestservice.getAllRequest();
		  }
		
		@RequestMapping(value="/requestTrip", method=RequestMethod.POST)
		public Request addRequest(@RequestBody AddRequestObject requestObject) {
		System.out.println("test " + requestObject.getAddress());
			return requestservice.AddRequest(requestObject);
		}
		@RequestMapping(value="/matchpassengers", method=RequestMethod.GET)
		public List<Request> matchTripPassengers(@RequestParam int offer_id){
			
			
			return requestservice.matchTripPassengers(offer_id);
		}
		
		@RequestMapping(value="/selectpassengers", method=RequestMethod.POST)
		public List<Request> addNewTrip(@RequestBody List<Request> tripPassengers ,@RequestParam int offer_id) {
			System.out.println("request body :" + tripPassengers);
			System.out.println("request param :" + offer_id);
		 	return requestservice.addNewTrip(tripPassengers ,offer_id);
		 	
		}

		
		
}
