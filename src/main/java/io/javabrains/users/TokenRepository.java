package io.javabrains.users;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface  TokenRepository  extends CrudRepository<Token ,Integer>{
	
@Modifying
@Transactional	
@Query(value="UPDATE user_token set notification_token = :notify_token where registeration_token = :register_token",nativeQuery = true)
	public void addDeviceToken(@Param("notify_token") String notify_token ,@Param("register_token") String register_token );

@Query(value="SELECT * FROM user_token WHERE user_id =:userId" , nativeQuery = true)
public Token getUserTokens(@Param("userId") long userId);
}
