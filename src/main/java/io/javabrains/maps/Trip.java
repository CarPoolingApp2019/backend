package io.javabrains.maps;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="trip")
public class Trip {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id ;
	private Long start_time;
private Long end_time;
private double cost;


@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
@OneToOne(fetch = FetchType.LAZY, optional = false)
@JoinColumn(name = "offer_id", nullable = false)
private Offer offer;

@JsonIgnore
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
@OneToMany(mappedBy = "trip", fetch = FetchType.EAGER,cascade = CascadeType.ALL)
private Set<Request> request ;

public Trip(Long start_time,Long end_time,Offer offer,double cost) {
	this.start_time=start_time;
	this.end_time=end_time;
	this.offer=offer;
	this.cost=cost;
	
}


public Trip() {
 id = 0;
}



public long getId() {
	return id;
}


public void setId(long id) {
	this.id = id;
}


public Long getStart_time() {
	return start_time;
}


public void setStart_time(Long start_time) {
	this.start_time = start_time;
}


public Long getEnd_time() {
	return end_time;
}


public void setEnd_time(Long end_time) {
	this.end_time = end_time;
}


public double getCost() {
	return cost;
}


public void setCost(double cost) {
	this.cost = cost;
}


public Offer getOffer() {
	return offer;
}


public void setOffer(Offer offer) {
	this.offer = offer;
}


public Set<Request> getRequest() {
	return request;
}


public void setRequest(Set<Request> request) {
	this.request = request;
}









}
