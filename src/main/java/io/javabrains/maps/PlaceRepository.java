package io.javabrains.maps;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import java.util.List;


public interface PlaceRepository extends CrudRepository<Place , Integer> {
	final int r = 6371;

	@Query(value="SELECT * FROM place WHERE acos(sin(:userlat) * sin(place.lat) + cos(:userlat) * cos(place.lat) * cos(place.lon - (:userlon))) * 6371 <= 5" ,nativeQuery = true)
	List<Place> findNearLocations(@Param("userlat") double userlat ,
			                      @Param("userlon") double userlon);
	
	@Query(value="SELECT * FROM place WHERE place.lat= :userlat AND place.lon= :userlon" ,nativeQuery = true)
	List<Place> ifLocationExists(@Param("userlat") double userlat ,
			                      @Param("userlon") double userlon);
	
	
}
