package io.javabrains.users;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.javabrains.maps.Place;
import io.javabrains.maps.Trip;
import io.javabrains.maps.Offer;
@Entity
@Table(name="vehicle")

public class Vehicle {
	  @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	  private String name;
	  private String modle;
	  private String number;
	  private String licence_car;
	  private String licence_driver;
	  @ManyToOne
	    @JoinColumn(name = "user_id")
		private User user;
	  
	   @JsonIgnore
		 @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
		 @OneToOne(fetch = FetchType.EAGER,cascade =  CascadeType.ALL,mappedBy = "vehicle")
		    private Offer offer;

	public Vehicle(String name, String modle, String number, String licence_car, 
			String licence_driver,User user) {
		this.name = name;
		this.modle = modle;
		this.number = number;
		this.licence_car = licence_car;
		this.licence_driver = licence_driver;
		this.user=user;
	
	}
	public Vehicle() {
	
	
	}
	public Vehicle(int id) {
		this.id=id;
	}
	

public int getId() {
		return id;
	}
	

public void setId(int id) {
		this.id = id;
	}
public String getName() {
		return name;
	}
public void setName(String name) {
		this.name = name;
	}
public String getModle() {
		return modle;
	}
public void setModle(String modle) {
		this.modle = modle;
	}
public String getNumber() {
		return number;
	}
public void setNumber(String number) {
		this.number = number;
	}
public String getLicence_car() {
		return licence_car;
	}
public void setLicence_car(String licence_car) {
		this.licence_car = licence_car;
	}


public String getLicence_driver() {
	return licence_driver;
}
public void setLicence_driver(String licence_driver) {
	this.licence_driver = licence_driver;
}
public User getUser() {
	return user;
}

public void setUser(User user) {
	this.user = user;
}
public Offer getOffer() {
	return offer;
}
public void setOffer(Offer offer) {
	this.offer = offer;
}
	 
	  
	
}


