package io.javabrains.maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="country")
public class Country {
	@Id
	 @GeneratedValue
	private String code ;
	private String name ;
	@JsonIgnore
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	    @OneToMany(mappedBy = "country",
	    fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	
	
	    private Set<Region> regions;

	    
	public Country(String code ,String name) {
		this.code=code;
		this.name=name;
	}
	public Country(){
		
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public Set<Region> getRegions() {
		return regions;
	}
	public void setRegions(Set<Region> regions) {
		this.regions = regions;
	}
	

}
