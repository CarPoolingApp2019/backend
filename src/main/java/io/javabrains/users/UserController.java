package io.javabrains.users;

import java.util.List;
import io.javabrains.users.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import Responses.LoginResponse;

import java.util.UUID;

@RestController
public class UserController {
	@Autowired
	private UserService userservice;
	@RequestMapping(value="/users", method=RequestMethod.GET)
	public List<User> getAllUsers() {
		  return userservice.getAllUsers();
	  }
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public LoginResponse Login(@RequestParam String phone) throws GlobalControllerExceptionHandler {
		String userPhone = phone.substring(2);
		System.out.println("phone" + phone);
		return userservice.login(userPhone);
	}
	@RequestMapping(method=RequestMethod.POST ,value="/signup")
	public LoginResponse addUser(@RequestBody User user) {
		System.out.println("signup" + user.getPhone());
		return userservice.addUser(user);
	}
	
//	public String generateToken() {
//		
//		UUID uuid = UUID.randomUUID();
//		
//	}
	
	
	
}
