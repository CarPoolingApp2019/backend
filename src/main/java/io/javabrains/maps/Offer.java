package io.javabrains.maps;
import io.javabrains.users.*;

import java.sql.Timestamp;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

@Entity
@Table(name="offer")
public class Offer {
	@Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
     private int id ;
	 private double longitude;
	 private double latitude;
	 private  long trip_actual_time;
	 private String address;
	
		 @ManyToOne
		    @JoinColumn(name = "destination_place_id")
			private Place place;
		 @ManyToOne
		    @JoinColumn(name = "user_id")
		 
			private User user;
		 @JsonIgnore
		 @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
		 @OneToOne(fetch = FetchType.EAGER,
		            cascade =  CascadeType.ALL,
		            mappedBy = "offer")
		    private Trip trip;
		 @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
		 @OneToOne(fetch = FetchType.LAZY, optional = false)
		 @JoinColumn(name="vehicle_id",nullable = false)
		 private Vehicle vehicle;

		
	public Offer(double longitude,double latitude,long trip_actual_time,String address
			,Place place,User user,Vehicle vehicle) {
		 this.longitude=longitude;
		 this.latitude=latitude;
		 this.trip_actual_time=trip_actual_time;
		 this.address=address;
		 this.place=place;
		 this.user=user;
		 this.vehicle=vehicle;
		
	 }
public Offer(int id) {
	this.id=id;
}
public Offer() {
	
}


	public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public Place getPlace() {
		return place;
	}


	public void setPlace(Place place) {
		this.place = place;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Trip getTrip() {
		return trip;
	}


	public void setTrip(Trip trip) {
		this.trip = trip;
	}
	public long getTrip_actual_time() {
		return trip_actual_time;
	}
	public void setTrip_actual_time(long trip_actual_time) {
		this.trip_actual_time = trip_actual_time;
	}
	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	
}
