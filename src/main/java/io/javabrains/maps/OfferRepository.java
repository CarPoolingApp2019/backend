package io.javabrains.maps;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import io.javabrains.users.User;
@Repository
public interface  OfferRepository  extends CrudRepository<Offer ,Integer>{
	
	

	
	
	@Query(value="SELECT * FROM offer WHERE offer.id = :offer_id" , nativeQuery = true)
	Offer getOfferById(@Param("offer_id") int offer_id);
	
	@Modifying
	@Transactional
	@Query(value="DELETE FROM offer WHERE offer.id = :offerID ", nativeQuery= true )
	public void DeleteOffer(@Param("offerID") int offerID);


}

