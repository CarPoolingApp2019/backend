package io.javabrains.users;
import io.javabrains.maps.*;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import io.javabrains.users.UserRepository;
import io.javabrains.maps.AddRequestObject;
import io.javabrains.maps.Place;
import io.javabrains.maps.Request;
import io.javabrains.users.AddVehicleobj;

@Service
public class VehicleService {
	@Autowired
	private VehicleRepository vehiclerepository;
	@Autowired
	private UserRepository userrepo;
	
	public Vehicle AddVehicle(AddVehicleobj addvehicle) {
		long user_id= userrepo.findUser(addvehicle.getRegisteration_token()).getId();
		User user = new User(user_id);
		
		Vehicle vehicle = new Vehicle(addvehicle.getName(),addvehicle.getModle(),addvehicle.getNumber(),
				addvehicle.getLicence_car(),addvehicle.getLicence_driver(),user);
		vehiclerepository.save(vehicle);
		System.out.println("test" +vehicle.getModle());
		return vehicle;
		
			
	}
	
	
	public void setDefultcar(int id) {
  long userid=vehiclerepository.selectUser(id).getUser().getId();
 
  vehiclerepository.setFalse(userid);
			 
		 vehiclerepository.setDefultcar(id);
	}
	
public List<Vehicle> findVehicleByregisterayion_token(String registeration_token) {
	
	List<Vehicle> vehicle = new ArrayList<>();
	vehiclerepository.findVehicleByregisterayion_token(registeration_token).
	forEach(vehicle ::add);
	return vehicle;
	
}
	
	
	
}
