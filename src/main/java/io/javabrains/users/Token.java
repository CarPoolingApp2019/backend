package io.javabrains.users;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name="user_token")
public class Token{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String  registeration_token;
    private String notification_token;
	public String getNotification_token() {
	return notification_token;
}
public void setNotification_token(String notification_token) {
	this.notification_token = notification_token;
}

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
	private User user;
	
	  public Token(String registeration_token ) {
	        this.registeration_token = registeration_token;
	    }
	
	  public Token(User user) {
		  
	        this.user= user;
	        this.registeration_token= generateToken();
	    }
	  public Token () {
		  
	  }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getToken() {
		return registeration_token;
	}

	public void setToken(String registeration_token) {
		this.registeration_token= registeration_token;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	private String generateToken() {
		UUID uuid = UUID.randomUUID();
		String registeration_token = uuid.toString();
		return registeration_token;
	}
	
	
}
//
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.Table;
//
//@Entity
//@Table(name="user_tokens")
//public class Tokens {
//	@Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//  private int id;
//  private String token;
//  private User user;
//  public Tokens() {
//	  
//  }
//public Tokens(int id, String token, User user) {
//	super();
//	this.id = id;
//	this.token = token;
//	this.user = user;
//}
//@Id
//@GeneratedValue(strategy = GenerationType.AUTO)
//public int getId() {
//	return id;
//}
//public void setId(int id) {
//	this.id = id;
//}
//public String getToken() {
//	return token;
//}
//public void setToken(String token) {
//	this.token = token;
//}
//@ManyToOne
//@JoinColumn(name = "user_id")
//public User getUser() {
//	return user;
//}
//public void setUser(User user) {
//	this.user = user;
//}
//
//  
//}
