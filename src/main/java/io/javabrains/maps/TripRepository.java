package io.javabrains.maps;


import io.javabrains.users.*;


import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;



public interface  TripRepository  extends CrudRepository<Trip ,Long>{
	
	@Query(value = "SELECT trip.* "
		+ "FROM trip "
		+"JOIN offer ON trip.offer_id=offer.id " 
		+ "JOIN users ON offer.user_id=users.id " 
		+"JOIN user_token ON user_token.user_id=users.id "
		+"WHERE (registeration_token= :registeration_token) " 
			 ,nativeQuery = true)
List<Trip> findTripByRegestration_token(@Param("registeration_token") String registeration_token);
	@Modifying
	@Transactional
	@Query(value ="UPDATE trip set start_time= :start_time WHERE trip.id= :id " ,nativeQuery = true)
	public void setStart_time(@Param("id") long id,@Param("start_time")long start_time);
	
	@Modifying
	@Transactional
	@Query(value ="UPDATE trip set end_time= :end_time WHERE trip.id= :id " ,nativeQuery = true)
	public void setEnd_time(@Param("id") long id,@Param("end_time")long start_time);
	
	@Query(value="SELECT * FROM trip WHERE trip.id= :tripId" , nativeQuery= true)
	public Trip findTripbyId(@Param("tripId") long tripId);
	
	@Query(value="SELECT * FROM trip WHERE trip.offer_id =:offerID", nativeQuery= true)
	public List<Trip> selectTripAttatchedToSpecificOffer(@Param("offerID") int offerId);
	
	@Modifying
	@Transactional
	@Query(value="DELETE FROM trip WHERE  trip.id= :trip_id", nativeQuery= true)
	public void deleteTrip(@Param("trip_id") long trip_id);
	
	
}



 
