package io.javabrains.maps;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
@Entity
@Table(name="city")
public class City {
	@Id
	private String code;
	private String name;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "region_code")
	
	@JsonBackReference
	private Region region;
	//one to many
	@JsonIgnore
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    @OneToMany(targetEntity=Place.class,mappedBy = "city",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Set<Place> places ;

	public City (String code,String name,Region region) {
	
	this.code=code;
	this.name=name;
	this.region=region;

	 
	}
	
	
	public City() {
		
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}


	public Set<Place> getPlaces() {
		return places;
	}


	public void setPlaces(Set<Place> places) {
		this.places = places;
	}


	
	
	
	}