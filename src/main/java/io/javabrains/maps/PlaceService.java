package io.javabrains.maps;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlaceService {
	@Autowired
	private PlaceRepository placerepository;

	public List<Place> getAllPlaces() {
		
		List<Place> places = new ArrayList<>();
		placerepository.findAll()
		.forEach(places :: add);
		return places;
	}
	
	public List<Place> findNearLocations(double lat , double lon){
		if(placerepository.ifLocationExists(lat, lon).size() !=0) {
			return  placerepository.ifLocationExists(lat, lon);
		}else {
		return placerepository.findNearLocations(lat, lon);
		}
		
	}


}

