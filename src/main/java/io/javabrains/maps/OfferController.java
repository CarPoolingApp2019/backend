package io.javabrains.maps;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import Responses.LoginResponse;
import io.javabrains.users.User;
@RestController
public class OfferController {
	
	
	@Autowired
	private OfferService offerservice;
	
	@RequestMapping(value="/offer", method=RequestMethod.GET)
	public List<Offer> getAllOffer() {
		  return offerservice.getAllOffer();
	  }
	@RequestMapping(value="/offerTrip", method=RequestMethod.POST)
	public Offer AddOffer(@RequestBody AddOfferobj addofferObj) {
System.out.println("registeration token"+addofferObj.getRegisteration_token());
		System.out.println("inside offer controller");
		return offerservice.AddOffer(addofferObj);
	}

	@RequestMapping(value="/cancleOffer", method=RequestMethod.POST)
	public void cancleOffer(@RequestParam int offer_id) {
		offerservice.cancleOffer(offer_id);
	}
	


}