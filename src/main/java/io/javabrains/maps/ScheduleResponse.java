package io.javabrains.maps;

import java.util.List;

public class ScheduleResponse {
List <UserTripResponse> ScheduleOffer;
List<UserTripResponse> ScheduleRequest;

public ScheduleResponse(List<UserTripResponse> scheduleOffer, List<UserTripResponse> scheduleRequest) {
	
	ScheduleOffer = scheduleOffer;
	ScheduleRequest = scheduleRequest;
}
public List<UserTripResponse> getScheduleOffer() {
	return ScheduleOffer;
}
public void setScheduleOffer(List<UserTripResponse> scheduleOffer) {
	ScheduleOffer = scheduleOffer;
}
public List<UserTripResponse> getScheduleRequest() {
	return ScheduleRequest;
}
public void setScheduleRequest(List<UserTripResponse> scheduleRequest) {
	ScheduleRequest = scheduleRequest;
}

}
