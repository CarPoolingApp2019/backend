package io.javabrains.users;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN, reason="No such Phone") // 403
class GlobalControllerExceptionHandler extends RuntimeException {
    
}