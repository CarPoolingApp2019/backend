package io.javabrains.maps;

public class AddRequestObject {
	
        double longitude;
        double latitude;
        long trip_actual_time;
        String registeration_token;        
        String address;
        int destination_place_id;
		public double getLongitude() {
			return longitude;
		}
		public AddRequestObject(double longitude, double latitude, long trip_actual_time, String registeration_token,
				String address, int destination_place_id) {
			this.longitude = longitude;
			this.latitude = latitude;
			this.trip_actual_time = trip_actual_time;
			this.registeration_token = registeration_token;
			this.address = address;
			this.destination_place_id = destination_place_id;
		}
		public AddRequestObject() {
			
		}
		public void setLongitude(double longitude) {
			this.longitude = longitude;
		}
		public double getLatitude() {
			return latitude;
		}
		public void setLatitude(double latitude) {
			this.latitude = latitude;
		}
		public long getTrip_actual_time() {
			return trip_actual_time;
		}
		public void setTrip_actual_time(long trip_actual_time) {
			this.trip_actual_time = trip_actual_time;
		}
		public String getRegisteration_token() {
			return registeration_token;
		}
		public void setRegisteration_token(String registeration_token) {
			this.registeration_token = registeration_token;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public int getDestination_place_id() {
			return destination_place_id;
		}
		public void setDestination_place_id(int destination_place_id) {
			this.destination_place_id = destination_place_id;
		}
        
        
        
}
