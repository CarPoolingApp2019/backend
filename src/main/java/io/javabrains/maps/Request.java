package io.javabrains.maps;
import io.javabrains.users.*;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="request")
public class Request {
	@Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	 private double longitude;
	 private double latitude;
	 private long trip_actual_time;
	 private String request_stauts;
	 private String address;
	 @ManyToOne
	    @JoinColumn(name = "destination_place_id")
		private Place place;
	 @ManyToOne(optional=true)
	    @JoinColumn(name = "trip_id")
		private Trip trip;
	 @ManyToOne
	    @JoinColumn(name = "user_id")
		private User user;
	 public  Request(double longitude,double latitude,long trip_actual_time,String request_stauts
			 ,String address,Place place,Trip trip,User user) {
		
		 this.trip_actual_time=trip_actual_time;
		 this.latitude=latitude;
		 this.longitude=longitude;
		 this.request_stauts=request_stauts;
		 this.address=address;
		 this.place=place;
		 this.trip=trip;
		 this.user=user;
	 }
	 public Request() {
		 
	 }
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(long longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(long latitude) {
		this.latitude = latitude;
	}
	
	public long getTrip_actual_time() {
		return trip_actual_time;
	}
	public void setTrip_actual_time(long trip_actual_time) {
		this.trip_actual_time = trip_actual_time;
	}
	public String getRequest_stauts() {
		return request_stauts;
	}
	public void setRequest_stauts(String request_stauts) {
		this.request_stauts = request_stauts;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Place getPlace() {
		return place;
	}
	public void setPlace(Place place) {
		this.place = place;
	}
	public Trip getTrip() {
		return trip;
	}
	public void setTrip(Trip trip) {
		this.trip = trip;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

}
