package io.javabrains.users;

public class AddVehicleobj {
	   String name;
	   String modle;
	   String number;
	   String licence_car;
	   String licence_driver;
	   String registeration_token;
	  
	public AddVehicleobj(String name, String modle, String number, String licence_car, String licence_driver,
			String registeration_token) {
		
		this.name = name;
		this.modle = modle;
		this.number = number;
		this.licence_car = licence_car;
		this.licence_driver = licence_driver;
		this.registeration_token = registeration_token;
	}
	public AddVehicleobj() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModle() {
		return modle;
	}

	public void setModle(String modle) {
		this.modle = modle;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getLicence_car() {
		return licence_car;
	}

	public void setLicence_car(String licence_car) {
		this.licence_car = licence_car;
	}

	public String getLicence_driver() {
		return licence_driver;
	}

	public void setLicence_driver(String licence_driver) {
		this.licence_driver = licence_driver;
	}

	public String getRegisteration_token() {
		return registeration_token;
	}

	public void setRegisteration_token(String registeration_token) {
		this.registeration_token = registeration_token;
	}
	
	  

}
