package io.javabrains;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.javabrains.maps.Trip;

import java.util.TimeZone;
import java.util.UUID;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class CarPoolingAppApplication {

	
	public static void main(String[] args) {
		UUID uuid = UUID.randomUUID();
		System.out.println("this is token" + uuid.toString());
		SpringApplication.run(CarPoolingAppApplication.class, args);
	}

}
