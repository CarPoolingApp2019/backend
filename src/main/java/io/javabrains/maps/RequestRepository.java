package io.javabrains.maps;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface  RequestRepository  extends CrudRepository<Request,Integer> {
	@Query(value ="SELECT * " 
			+"FROM trip " 
			+"JOIN request ON request.trip_id=trip.id " 
			+"JOIN users ON request.user_id=users.id " 
			+"JOIN user_token ON user_token.user_id=users.id " 
			+"WHERE (registeration_token= :registeration_token) " ,nativeQuery = true)
	List<Request> findRequestTripByRegestration_token(@Param("registeration_token") String registeration_token);
	
	

	@Query(value="SELECT * FROM request where request.destination_place_id = :destinationID AND request.trip_id IS NULL AND request.trip_actual_time= :tripTime" ,nativeQuery = true)
	List<Request> matchTripPassengers(@Param("destinationID") int destinationID,@Param("tripTime") Long tripTime);
	
@Modifying
@Transactional
@Query(value="UPDATE request set trip_id = :trip_id where id = :request_id",nativeQuery = true)
	void connectRequestToTrip(@Param("trip_id") long trip_id , @Param("request_id") long request_id);


@Query(value="SELECT * FROM request WHERE request.trip_id = :tripId",nativeQuery = true)
List<Request> getRequestbyTripId(@Param("tripId") long tripId);

@Modifying
@Transactional
@Query(value="UPDATE request set trip_id = null where trip_id = :tripId",nativeQuery = true)
void unconnectRequestToTrip(@Param("tripId") long tripId);












}