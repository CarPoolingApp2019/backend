package io.javabrains.firebase.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

//service to send a notification from API to the mobile device which accepts server secret key
@Service
public class AndroidPushNotificationsService {
	private static final String FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send";
	private static final String FIREBASE_SERVER_KEY = "AAAALDr9Pk8:APA91bFXj-FEGOajaqjtiGUChPf7OJrU06pd_0vCuCFacFyD75y4MxI9VcmGEurN1bJCCRwfijRmXPfNx7p5LQh8BeLkMkFlOS6rMDDvFXTbPettVRWWwFo9E2dPQ1zeOtZhaCoPbro5";

	
	public static String sendPushNotification(String deviceToken, String title, String body) throws IOException {
		System.out.println("sendNotification Service");
		String result = "";
		URL url = new URL(FIREBASE_API_URL);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Authorization", "key=" + FIREBASE_SERVER_KEY);
		conn.setRequestProperty("Content-Type", "application/json");

		JSONObject json = new JSONObject();
		try {

			json.put("to", deviceToken.trim());

			JSONObject data = new JSONObject();
			data.put("Key-1", "data");
			data.put("Key-2", "data");
			json.put("data", data);
			JSONObject info = new JSONObject();
			info.put("title", title); // Notification title
			info.put("body", body); // Notification
			info.put("message", "hello user"); // body
			json.put("notification", info);
			System.out.println("notification Request" + json);
		}catch (JSONException e1) {
			e1.printStackTrace();
		}
		try {
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(json.toString());
			wr.flush();

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println("this is output"+output);
			}
			result = "succcess";
		}catch (Exception e) {
			e.printStackTrace();
			result = "failure";
		}
		System.out.println("FCM Notification is sent "+result);

		return result;

	}

}
