package io.javabrains.maps;
import io.javabrains.users.*;

import java.util.List;

import io.javabrains.maps.TripRepository;
public class UserTripResponse {
private Long start_time;
private Long end_time;
private String address;
private String destination;
private double cost;
private int IsOffer;
private  long id;
private int offer_id;

public UserTripResponse(String address ,Long end_time,Long start_time,String destination,double cost,int IsOffer,long id,int offer_id) {
	 
	this.address=address;
	 this.end_time=end_time;
	 this.start_time=start_time;
	 this.cost=cost;
	 this.destination=destination;
	 this.IsOffer=IsOffer;
	 this.id=id;
	 this.offer_id=offer_id;

 }

public Long getStart_time() {
	return start_time;
}

public void setStart_time(long start_time) {
	this.start_time = start_time;
}

public Long getEnd_time() {
	return end_time;
}

public void setEnd_time(Long end_time) {
	this.end_time = end_time;
}

public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public String getDestination() {
	return destination;
}

public void setDestination(String destination) {
	this.destination = destination;
}

public double getCost() {
	return cost;
}

public void setCost(double cost) {
	this.cost = cost;
}

public int getIsOffer() {
	return IsOffer;
}

public void setIsOffer(int isOffer) {
	IsOffer = isOffer;
}

public long getId() {
	return id;
}

public void setId(long id) {
	this.id = id;
}

public int getOffer_id() {
	return offer_id;
}

public void setOffer_id(int offer_id) {
	this.offer_id = offer_id;
}



}
