package io.javabrains.maps;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Responses.LoginResponse;
import io.javabrains.firebase.service.MobileController;
import io.javabrains.users.Token;
import io.javabrains.users.User;
import io.javabrains.users.UserRepository;
import javassist.expr.NewArray;

@Service
public class RequestService {
	@Autowired
	private RequestRepository requestrepository;
	@Autowired
	private OfferRepository offerrRepo;
	
	@Autowired
	private UserRepository userepo;
	@Autowired
	private PlaceRepository placerepo;
	@Autowired
	private TripService tripservice;
	@Autowired
	private MobileController mobilecontroller;
	


	public List<Request> getAllRequest(){
		List<Request> request = new ArrayList<>();
		requestrepository.findAll()
		.forEach(request :: add);
		
		return request;

}
	
	public Request AddRequest(AddRequestObject addRequestObj) {
	long user_id= userepo.findUser(addRequestObj.getRegisteration_token()).getId();
	User user = new User(user_id);
	Place place = new Place(addRequestObj.getDestination_place_id());
	String requestStatus = "pending";
	Request request = new Request(addRequestObj.longitude , addRequestObj.getLatitude(),addRequestObj.getTrip_actual_time(),requestStatus,addRequestObj.getAddress(),place,null,user);
	requestrepository.save(request);
	System.out.print("sabrin "+request.getAddress());
	return request;
	
		
	}
	
	public List<Request> matchTripPassengers(int offer_id){
	
		Offer offer = offerrRepo.getOfferById(offer_id);
		return requestrepository.matchTripPassengers(offer.getPlace().getId(), offer.getTrip_actual_time());
	}

	public List<Request> addNewTrip(List<Request> tripPassengers, int offer_id) {
		System.out.println("request service and set trip id in request");
		Trip newTrip =tripservice.addNewTrip(offer_id);
		for(Request request : tripPassengers) {
		 requestrepository.connectRequestToTrip(newTrip.getId(), request.getId());
		}
		System.out.println("new trip id" + newTrip.getId());
		mobilecontroller.sendNotifications(tripPassengers , newTrip.getId());
		
	return	requestrepository.getRequestbyTripId(newTrip.getId());
		
	}
	
	
	
	
	
	
	
	}