package io.javabrains.firebase.service;

import java.util.ArrayList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import io.javabrains.maps.Request;
import io.javabrains.maps.Trip;
import io.javabrains.maps.TripRepository;
import io.javabrains.maps.Offer;
import io.javabrains.maps.OfferRepository;
import io.javabrains.users.UserRepository;
import io.javabrains.users.*;

@Service
public class PassengersDeviceTokens {
@Autowired
	private TokenRepository tokenrepo;
@Autowired
private TripRepository triprepo;
@Autowired
private OfferRepository offerrepo;
@Autowired
private UserRepository userrepo;
	
	public List<String> getpassengersTokens(List<Request> selectedPassengers){
		List<String> notificationTokens = new ArrayList<String>();
		for(Request request : selectedPassengers) {
			long user_id = request.getUser().getId();
			System.out.println("user_id: "+user_id);
			Token userTokens = tokenrepo.getUserTokens(user_id);
			String notification_token = userTokens.getNotification_token();
			notificationTokens.add(notification_token);
		}
		return notificationTokens;
	}
	
	
	public String getTripDriverName(long trip_id) {
//		Request passenger = selectedPassengers.get(0);
	Trip trip = triprepo.findTripbyId(trip_id);
		Offer offer = offerrepo.getOfferById(trip.getOffer().getId());
		User user = userrepo.findUserByID(offer.getUser().getId());
		String driverName = user.getFirst_name() + " " +user.getSecond_name();
		return driverName;
		
		
	}
}
